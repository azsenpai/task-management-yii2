USING
-----
1. `git clone https://coder_iitu@bitbucket.org/coder_iitu/task-management-yii2.git`

2. `cd task-management-yii2`

3. `composer install`

4. `yii serve/index`

5. Check the address http://localhost:8080

DB schema
---------

![db-schema.png](https://bitbucket.org/repo/GjjrkL/images/1281584676-db-schema.png)