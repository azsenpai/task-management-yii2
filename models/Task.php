<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $description
 * @property boolean $completed
 * @property integer $due_date
 * @property integer $created_at
 * @property integer $updated_at
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'description'], 'required'],
            [['project_id'], 'integer'],
            [['description'], 'string'],
            [['completed'], 'boolean'],
            [['description'], 'string', 'max' => 255],

            [['due_date'], 'safe'],

            ['completed', 'default', 'value' => false],

            ['due_date', 'filter', 'filter' => function($value) {
                if (!empty($value)) {
                    return strtotime($value);
                }

                return $value;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'description' => 'Description',
            'completed' => 'Completed',
            'due_date' => 'Due Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $project = $this->project;

        if (!empty($project->due_date)) {
            if (empty($this->due_date)) {
                $this->updateAttributes(['due_date' => $project->due_date]);
            } else if ($this->due_date > $project->due_date) {
                $project->updateAttributes(['due_date' => $this->due_date]);
            }
        }

        if (isset($changedAttributes['completed'])) {
            $project->trigger(Project::EVENT_CHECK_COMPLETE);
        }
    }

    /**
     *
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
