<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $title
 * @property boolean $completed
 * @property integer $due_date
 * @property integer $created_at
 * @property integer $updated_at
 */
class Project extends \yii\db\ActiveRecord
{
    const EVENT_CHECK_COMPLETE = 'checkComplete';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     *
     */
    public static function all()
    {
        $projects = self::find()
            ->select(['id', 'title'])
            ->asArray()
            ->all();

        return ArrayHelper::map($projects, 'id', 'title');
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_CHECK_COMPLETE, [$this, 'checkComplete']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['completed'], 'boolean'],
            [['due_date'], 'safe'],
            [['title'], 'string', 'max' => 255],

            ['completed', 'default', 'value' => false],

            ['due_date', 'filter', 'filter' => function($value) {
                if (!empty($value)) {
                    return strtotime($value);
                }

                return null;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'completed' => 'Completed',
            'due_date' => 'Due Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Task::deleteAll(['project_id' => $this->id]);

            return true;
        }

        return false;
    }

    /**
     *
     */
    public function checkComplete($event)
    {
        if (!$this->completed) {
            if ($this->numberOfUncompletedTasks == 0) {
                $this->updateAttributes(['completed' => true]);
            }
        } else {
            if ($this->numberOfUncompletedTasks > 0) {
                $this->updateAttributes(['completed' => false]);
            }
        }
    }

    /**
     *
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['project_id' => 'id']);
    }

    /**
     *
     */
    public function getNumberOfTasks()
    {
        return $this->getTasks()
            ->count();
    }

    /**
     *
     */
    public function getNumberOfCompletedTasks()
    {
        return $this->getTasks()
            ->andWhere(['completed' => true])
            ->count();
    }

    /**
     *
     */
    public function getNumberOfUncompletedTasks()
    {
        return $this->getTasks()
            ->andWhere(['completed' => false])
            ->count();
    }

    /**
     *
     */
    public function getPercentComplete()
    {
        $numberOfTasks = $this->numberOfTasks;
        $numberOfCompletedTasks = $this->numberOfCompletedTasks;

        if ($numberOfTasks == 0) {
            return 100;
        }

        return $numberOfCompletedTasks / $numberOfTasks * 100;
    }
}
