<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use app\models\Project;

/**
 * ProjectSearch represents the model behind the search form about `app\models\Project`.
 */
class ProjectSearch extends Project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'due_date'], 'safe'],
            [['completed'], 'boolean'],

            ['due_date', 'filter', 'filter' => function($value) {
                if (!empty($value)) {
                    return date('Y-m-d', strtotime($value));
                }
                return $value;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'completed' => $this->completed,
            'date(due_date, "unixepoch", "localtime")' => $this->due_date,
        ]);

        $query->andFilterWhere([]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        //\yii\helpers\VarDumper::dump($query->createCommand()->rawSql, 10, true);
        //exit();

        return $dataProvider;
    }
}
