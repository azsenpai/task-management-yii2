<?php

use yii\db\Migration;

/**
 *
 */
class m170305_155210_project extends Migration
{
    const TABLE_NAME = 'project';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'completed' => $this->boolean(),
            'due_date' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
