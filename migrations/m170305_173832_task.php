<?php

use yii\db\Migration;

/**
 *
 */
class m170305_173832_task extends Migration
{
    const TABLE_NAME = 'task';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'description' => $this->string()->notNull(),
            'completed' => $this->boolean(),
            'due_date' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            sprintf('idx-%s-project_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'project_id'
        );

        /*$this->addForeignKey(
            sprintf('fk-%s-project_id', self::TABLE_NAME),
            self::TABLE_NAME,
            'project_id',
            'project',
            'id'
        );*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex(
            sprintf('idx-%s-project_id', self::TABLE_NAME),
            self::TABLE_NAME
        );

        /*$this->dropForeignKey(
            sprintf('fk-%s-project_id', self::TABLE_NAME),
            self::TABLE_NAME
        );*/

        $this->dropTable(self::TABLE_NAME);
    }
}
