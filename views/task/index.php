<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;
use app\models\Project;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => Yii::$app->user->isGuest ? null : $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'project_id',
                'filter' => Project::all(),
                'value' => 'project.title',
            ],
            'description',
            [
                'attribute' => 'completed',
                'filter' => [
                    1 => 'Yes',
                    0 => 'No',
                ],
                'format' => 'boolean',
            ],
            [
                'attribute' => 'due_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'due_date',
                    'options' => ['class' => 'form-control'],
                ]),
                'format' => 'date',
            ],

            ['class' => 'yii\grid\ActionColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{complete}',

                'buttons' => [
                    'complete' => function($url, $model, $key) {
                        if (!$model->completed) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>',
                                $url, 
                                [
                                    'title' => 'Complete',
                                    'data' => [
                                        'method' => 'post',
                                    ],
                                ]
                            );
                        }

                        return Html::a(
                            '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>',
                            $url, 
                            [
                                'title' => 'Uncomplete',
                                'data' => [
                                    'method' => 'post',
                                ],
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>
</div>
