<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'completed:boolean',
            'due_date:date',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

    <h2>Tasks:</h2>

    <p>
        <?= Html::a('Create Task', ['task/create', 'project_id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $taskDataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'description',
            'completed:boolean',
            'due_date:date',
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'task',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'task',

                'template' => '{complete}',

                'buttons' => [
                    'complete' => function($url, $model, $key) {
                        if (!$model->completed) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>',
                                $url, 
                                [
                                    'title' => 'Complete',
                                    'data' => [
                                        'method' => 'post',
                                    ],
                                ]
                            );
                        }

                        return Html::a(
                            '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>',
                            $url, 
                            [
                                'title' => 'Uncomplete',
                                'data' => [
                                    'method' => 'post',
                                ],
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>

</div>
